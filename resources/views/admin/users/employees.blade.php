@extends('layouts.admin')

@section('content')


	<div class="col-md-10">
		<div class="admin-block">
			<div class="admin-header">
				<h3 class="">Employees</h3>
			</div>
			<div class="admin-body">

				@if(count($employees) > 0)
					<table class="table ui col-12 table-responsive-sm">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Role</th>
							<th>Added in</th>
							<th>Last login</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						@foreach($employees as $employee)
							<tr class="{{ Auth()->user()->id === $employee->id ? "positive" : "" }}">
								<td>{{ $employee->id }}</td>
								<td>{{ $employee->name }}</td>
								<td>{{ $employee->email}}</td>
								<td>{{ $employee->role}}</td>
								<td>{{ $employee->created_at}}</td>
								<td>{{ $employee->last_login }}</td>
								<td><a href="{{ route("admin.users.edit", $employee->id) }}">Edit</a></td>
							</tr>
						@endforeach
						</tbody>
					</table>
					{{ $employees->links() }}
				@else

					<h4>There are no employees</h4>

				@endif
			</div>
		</div>
	</div>

@endsection
